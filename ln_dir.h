#ifndef LN_DIR_H
#define LN_DIR_H

#ifdef __cplusplus
extern "C"
#endif
int mkdir_parent(const char *s, mode_t mode);

#endif
