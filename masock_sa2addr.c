#include "config.h"

#undef _GNU_SOURCE
#define _GNU_SOURCE	// to expose nonstandard members in in.h structures to
			// fix compilation in strict conformance mode on Linux

#include "masock.h"
#include "critmem.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#ifndef __LCLINT__
#include <arpa/inet.h>
#endif /* not __LCLINT__ */
#include <netdb.h>

#include <string.h>

#ifdef WITH_DMALLOC
#include <dmalloc.h>
#endif

#include <errno.h>

/**  */
/*@null@*/ /*@only@*/ char *
masock_sa2addr(const struct sockaddr *sa, const socklen_t len)
{
    char buf[NI_MAXHOST];
    int rc = getnameinfo(sa, len, buf, sizeof buf, NULL, 0, NI_NUMERICHOST);
    if (0 == rc) return critstrdup(buf, "masock_sa2addr");
    return NULL;
}
